# Subject Zippy Test

This is a test of a CSS-only solution to laying out the subject zippies using
only CSS. The [Tailwind CSS](https://tailwindcss.com/) framework was used for
its utility classes but ultimately this solution simply relies on [CSS
Grid](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout).

The source files are in the `public` directory.
